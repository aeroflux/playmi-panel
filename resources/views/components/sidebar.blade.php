<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="{{ URL::asset('img/app-logo.png')}}" height="30" alt="Playmi Logo">
        <span class="brand-text font-weight-normal">| {{ $title }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link active">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Video
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/videos" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Video</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/video/upload" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Unggah Video</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Kanal
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/channels" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Kanal</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/channel/create" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Buat Kanal</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="/categories" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Kategori
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="pages/widgets.html" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Statistik
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/recommendations" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Rekomendasi
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/reports" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Laporan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/opinion" class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Saran/Masukan
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">
                        <i class="nav-icon fas fa-th"></i>
                        <p>
                            Artikel
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/articles" class="nav-link active">
                                <i class="far fa-circle nav-icon"></i>
                                <p>List Artikel</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/article/create" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Buat Artikel</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-header">Akun</li>
                <li class="nav-item">
                    <a href="/user/admin" class="nav-link">Admin</a>
                </li>
                <li class="nav-item">
                    <a href="/user/logout" class="nav-link">Logout</a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
