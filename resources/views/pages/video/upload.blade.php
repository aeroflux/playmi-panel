<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('css/adminlte.min.css') }}">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ URL::asset('css/OverlayScrollbars.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Tempus Dominus -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css"/>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/master-style.css') }}">

    <title>{{ $title }}</title>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <!-- Main Sidebar Container -->
@component('components.sidebar')
    @slot('title')
        {{ $title }}
    @endslot
@endcomponent

<!-- Content Wrapper. Contains pages content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid form">
                <div class="row">
                    <h2>Tambah Video Baru</h2>
                </div>
                <form method="POST" action="/video/upload">
                    @csrf
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Judul Video</label>
                                <input name="title" type="text" class="form-control" placeholder="Masukkan judul video">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Link Video</label>
                                <input name="link" type="text" class="form-control" placeholder="Masukkan link video">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea name="description" class="form-control" rows="5"
                                          placeholder="Masukkan deskripsi video"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <div class="form-group">
                                <label>Kanal</label>
                                <input name="channel" type="text" class="form-control"
                                       placeholder="Masukkan nama kanal">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <!-- select -->
                            <div class="form-group">
                                <label>Pilih Kategori</label>
                                <select name="category" class="form-control">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                    <option>option 5</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <!-- select -->
                            <div class="form-group">
                                <label>Pilih Subkategori</label>
                                <select name="subcategory" class="form-control">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                    <option>option 5</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <!-- select -->
                            <div class="form-group">
                                <label>Pilih Label</label>
                                <select name="label" class="form-control">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                    <option>option 5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <!-- radio -->
                            <div class="form-group">
                                <label>Waktu Unggah</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="uploadOption">
                                    <label class="form-check-label">Segera</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="uploadOption" checked>
                                    <label class="form-check-label">Atur Jadwal</label>
                                </div>
                                <div class="input-group date" id="videoScheduleDatetimePicker"
                                     data-target-input="nearest">
                                    <input name="uploadScheduleDatetime" type="text"
                                           class="form-control datetimepicker-input"
                                           data-target="#videoScheduleDatetimePicker"/>
                                    <div class="input-group-append" data-target="#videoScheduleDatetimePicker"
                                         data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(function () {
                                    $('#videoScheduleDatetimePicker').datetimepicker();
                                });
                            </script>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <a class="btn btn-primary" role="button" type="submit" style="float: right">Unggah</a>
                            <a class="btn btn-outline-primary" style="float: right">Simpan Draf</a>
                            <a class="btn btn-outline-danger" style="float: right">Batal</a>
                        </div>
                    </div>
                </form>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 3.0.0
        </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ URL::asset('js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('js/adminlte.js') }}"></script>
<!-- Font Awesome -->
<script src="https://kit.fontawesome.com/c6dd958ec6.js" crossorigin="anonymous"></script>

<script src="{{ URL::asset('js/moment.js') }}"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/js/tempusdominus-bootstrap-4.min.js"></script>
</body>
</html>
