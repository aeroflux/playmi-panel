<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL::asset('css/adminlte.min.css') }}">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ URL::asset('css/OverlayScrollbars.min.css') }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/master-style.css') }}">

    <!-- summernote -->
    <link rel="stylesheet" href="{{ URL::asset('css/summernote-bs4.css') }}">

    <title>{{ $title }}</title>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
  <!-- Main Sidebar Container -->
  @component('components.sidebar')
      @slot('title')
          {{ $title }}
      @endslot
  @endcomponent

  <!-- Content Wrapper. Contains pages content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid form">
          <div class="row">
              <h2>Buat Artikel Baru</h2>
          </div>
          <form method="POST" action="/article/submit">
              @csrf
              <div class="row">
                  <div class="col-md">
                      <div class="form-group">
                          <label>Judul Artikel</label>
                          <input name="article_title" type="text" class="form-control"
                                 placeholder="Masukkan judul artikel">
                      </div>
                      <div class="form-group">
                          <label>Konten Artikel</label>
                          <textarea name="article_content" class="textareaArticle"
                                    placeholder="Masukan konten artikel"
                                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                      </div>
                  </div>
              </div>
          </form>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ URL::asset('js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ URL::asset('js/adminlte.js') }}"></script>
<!-- Font Awesome -->
<script src="https://kit.fontawesome.com/c6dd958ec6.js" crossorigin="anonymous"></script>

<!-- Summernote -->
<script src="{{ URL::asset('js/summernote-bs4.js') }}"></script>
<script>
    $(function () {
        // Summernote
        $('.textareaArticle').summernote()
    })
</script>
</body>
</html>
