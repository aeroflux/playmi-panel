<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/user/{username}', 'UserController@show');

Route::get('/', 'VideoController@index');
Route::get('/videos', 'VideoController@index');
Route::get('/video/upload', 'VideoController@create');
Route::post('/video/upload', 'VideoController@store');

Route::get('/channels', 'ChannelController@index');
Route::get('/channel/{id}/detail', 'ChannelController@show');
Route::get('/channel/create', 'ChannelController@create');

Route::get('/categories', 'CategoryController@index');
Route::get('/categories/{id}/subcategory', 'CategoryController@showSubcategory');

Route::get('/recommendations', 'RecommendationController@index');
Route::get('/reports', 'ReportController@index');
Route::get('/opinions', 'OpinionController@index');

Route::get('/articles', 'ArticleController@index');
Route::get('/articles/{id}', 'ArticleController@show');
Route::get('/article/create', 'ArticleController@create');
Route::get('/articles/{id}/edit', 'ArticleController@edit');
